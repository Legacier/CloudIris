CloudIris
==========================

[![image](https://img.shields.io/badge/python-3.5%2B-blue.svg)](https://www.python.org/)
[![image](https://img.shields.io/badge/Django-2.1-blue.svg)](https://docs.djangoproject.com/en/2.1/)
[![image](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://fsfe.org/campaigns/gplv3/gplv3.es.html)
[![build](https://gitlab.com/Legacier/cloudiris/badges/master/build.svg)](https://gitlab.com/Legacier/CloudIris/pipelines)

**Prueba de concepto - WebApp para clasificación de especies Iris. [Click aquí para ver demostración](http://13.58.18.21:8082/).**


![image](https://i2.wp.com/ligdigonzalez.com/wp-content/uploads/2018/06/iris_types.jpg?w=900)

Instalación
------------

Se recomienda crear un entorno virtual para el proyecto.
``` {.sourceCode .bash}
$ python -m venv irisenv
```

Posteriormente, es necesario activar el nuevo entorno virtual.

##### GNU/Linux
``` {.sourceCode .bash}
$ source irisenv/bin/activate
```

##### Windows
``` {.sourceCode .bash}
$ irisenv\Scripts\activate.bat
```

Actualizar gestor de paquetes *pip* a la última versión.

``` {.sourceCode .bash}
$ python -m pip install --upgrade pip
```

Instalar todos los paquetes de Python necesarios.


``` {.sourceCode .bash}
$ pip3 install -r requirements.txt
```

Iniciar el servidor de Django sobre el puerto 8000. Se puede especificar un número diferente de puerto. 

``` {.sourceCode .bash}
$ python manage.py runserver 8000
```

Iniciar el navegador en la dirección <http://127.0.0.1:8000/>. Se desplegará el menú principal de la aplicación web.

![image](https://preview.ibb.co/mbQAaq/ndice.jpg)


Documentación
-------------

La API de CloudIris se encuentra completamente documentada en
<https://cloudiris.readthedocs.io/>.

Estado del desarrollo
-------------

- [x] Vistas
    - [x] Menú principal
    - [x] Descripciones extendidas especies Iris
    - [x] Formulario API Predict
    - [ ] Estadísticas
- [ ] API
    - [ ] Predict
    - [ ] Analytics


