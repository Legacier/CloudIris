from django.shortcuts import render, HttpResponse
import json
import random


def home(request):
    return render(request, 'Iris/index.html')


def setosa(request):
    return render(request, 'Iris/setosa.html')


def versicolor(request):
    return render(request, 'Iris/versicolor.html')


def virginica(request):
    return render(request, 'Iris/virginica.html')


def apiform(request):
    return render(request, 'Iris/apiform.html')


def analytics(request):
    return render(request, 'Iris/historic.html')


def api(request):
    if request.method == 'POST':
        # Debug Response.
        return HttpResponse(json.dumps({'Class': random.choice(['Setosa', 'Versicolor', 'Virginica'])}))
    else:
        return render(request, 'Iris/apiform.html')
